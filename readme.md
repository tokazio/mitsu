# Mitsubishi's MELCloud java binding

https://bitbucket.org/tokazio/mitsu/addon/pipelines/home#!/results/page/1

[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=tokazio_mitsu&metric=reliability_rating)](https://sonarcloud.io/summary/new_code?id=tokazio_mitsu)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=tokazio_mitsu&metric=security_rating)](https://sonarcloud.io/summary/new_code?id=tokazio_mitsu)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=tokazio_mitsu&metric=bugs)](https://sonarcloud.io/summary/new_code?id=tokazio_mitsu)

Java 11 because of Sonar not compatible to java 17. Using MELCloud https://app.melcloud.com.

## Program 1

Check every 5 minutes if a clim needs to be started or stopped. If the given clim is in the given period, we check if
it's powered, if it's not and the room temperature is lower than 20�C and the exterior temperature is higher than -6�C
we start the device. If the device is working outside a given period, it will be shutdown.

## Program 2

Check temperatures every 10 minutes for logging to csv.

## Run

Use the Runner class.

# HTTP Server

## Show temperature graph

* http://127.0.0.1:1311/nous
* http://127.0.0.1:1311/elisa
* http://127.0.0.1:1311/marilou
* http://127.0.0.1:1311/bureau
* http://127.0.0.1:1311/salon (custom ESP8266)

## TODO

* include ESP8266 temp source code
* 1 thread by program allowing different/independent scheduling times
* Allow spi programs
* Allow hot loading/unloading programs
* Allow external program configuration

# Webographie

http://mgeek.fr/blog/un-peu-de-reverse-engineering-sur-melcloud