package fr.tokazio.mitsu;

import fr.tokazio.mitsu.core.Creds;
import fr.tokazio.mitsu.core.MonTruc;
import fr.tokazio.mitsu.mdns.Mdns;
import fr.tokazio.mitsu.mitsubishi.api.Clim;
import fr.tokazio.mitsu.mitsubishi.core.MitsubishiService;
import fr.tokazio.mitsu.programs.ScheduledProgram;
import fr.tokazio.mitsu.server.NettyHttpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class Runner {

    private static final Logger LOGGER = LoggerFactory.getLogger(Runner.class);

    private static final MonTruc monTruc = new MonTruc();

    public static void main(String[] args) throws IOException, InterruptedException {

        new Mdns().lookup();

        serverHttp();

        tempSalon();

        clims();
    }

    private static void tempSalon() {
        new Timer().scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                try {
                    final BigDecimal temp = new BigDecimal(monTruc.get(Creds.get("esptemphttp")));
                    LOGGER.info("* Salon à {}°C", temp);
                    FileAppender.timedAppendTo("temps.csv", "Salon\t" + temp);
                } catch (Exception e) {
                    LOGGER.warn("Error getting Salon temperature", e);
                }
            }
        }, 0, 1000L * 60 * 10);
    }

    private static void serverHttp() {
        new NettyHttpServer("mitsu-server", 1311)
                .get("/nous", new CourbesResource("Chambre Nous"))
                .get("/salon", new CourbesResource("Salon"))
                .get("/marilou", new CourbesResource("Chambre Marilou"))
                .get("/elisa", new CourbesResource("Chambre Elisa"))
                .get("/bureau", new CourbesResource("Bureau"))
                .startAsync();
    }

    private static void clims() throws IOException, InterruptedException {
        final MitsubishiService mitsubishiService = new MitsubishiService();
        mitsubishiService
                /*
                .addProgramme(new ProgramPlage("Heures creuses")
                        .addPlages(List.of(
                                PlageHorraire.of("13:55", "14:30"),//plage officielle 12:30-14:30 mais on ne chauffe que sur les 30 dernières minutes on démarre à 5min prêt
                                PlageHorraire.of("21:25", "22:00")//plage officielle 20:30-22:00 mais on ne chauffe que sur les 30 dernières minutes on démarre à 5min prêt
                                //on n'utilise pas celle-là car on dort    PlageHorraire.of("01:30", "05:00")
                        ))
                        .addClims(List.of(
                                Clim.of("Chambre Nous", Heat.of(24, SLOW)),
                                Clim.of("Chambre Marilou", Heat.of(23, SLOW)),
                                Clim.of("Chambre Elisa", Heat.of(22, SLOW)))))
                 */
                .addProgramme(new ScheduledProgram<Clim>("Températures", 10, TimeUnit.MINUTES)

                        .execute(clim -> FileAppender.timedAppendTo("temps.csv", clim.getName() + "\t" + clim.getRoomTemperature() + "\t" + clim.getExternalTemperature() + "\t" + clim.isPowered()))
                        .addAll(List.of(
                                        mitsubishiService.climFrom("Chambre Nous"),
                                        mitsubishiService.climFrom("Chambre Marilou"),
                                        mitsubishiService.climFrom("Chambre Elisa"),
                                        mitsubishiService.climFrom("Bureau")
                                )
                        ))
                .start();
    }

}
