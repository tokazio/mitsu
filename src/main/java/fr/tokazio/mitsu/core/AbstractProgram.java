package fr.tokazio.mitsu.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractProgram<T> implements Program {

    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractProgram.class);
    protected final List<T> list = new ArrayList<>();
    private final String name;

    protected AbstractProgram(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public <P extends AbstractProgram<T>> P add(final T obj) {
        if (obj != null) {
            this.list.add(obj);
        } else {
            LOGGER.warn("Can't add null element to a program");
        }
        return (P) this;
    }

    public <P extends AbstractProgram> P addAll(final List<T> list) {
        for (T obj : list) {
            add(obj);
        }
        return (P) this;
    }

}
