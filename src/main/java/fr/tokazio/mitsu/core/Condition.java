package fr.tokazio.mitsu.core;

public interface Condition<T> {

    static <V> ConditionBuilder<V> create(final String message) {
        return new ConditionBuilder<>(message);
    }

    boolean verified(T data);

    default String getMessage() {
        return "";
    }
}
