package fr.tokazio.mitsu.core;

public interface Hardware {

    String getName();

    boolean isPowered();

}
