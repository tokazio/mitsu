package fr.tokazio.mitsu.core;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.SocketTimeoutException;

public class MonTruc {

    private static final Logger LOGGER = LoggerFactory.getLogger(MonTruc.class);

    private final OkHttpClient client;

    public MonTruc() {
        client = new OkHttpClient.Builder().build();
    }

    public String get(final String url) throws IOException {
        final Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        LOGGER.debug("GETTING from {}", url);
        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        } catch (SocketTimeoutException e) {
            LOGGER.warn("Ne semble pas connecté", e);
            throw e;
        }
    }
}
