package fr.tokazio.mitsu.core;

import fr.tokazio.mitsu.mitsubishi.webapi.FanSpeed;
import fr.tokazio.mitsu.mitsubishi.webapi.OperationMode;

import java.math.BigDecimal;

public class Heat {

    private Heat() {
        super();
    }

    public static Temperature of(final int temp, final FanSpeed speed) {
        return new Temperature() {

            @Override
            public OperationMode getMode() {
                return OperationMode.HEATING;
            }

            @Override
            public BigDecimal getValue() {
                return new BigDecimal(String.valueOf(temp));
            }

            @Override
            public FanSpeed speed() {
                return speed;
            }
        };
    }

    public static Temperature of(final String temp, final FanSpeed speed) {
        return new Temperature() {

            @Override
            public OperationMode getMode() {
                return OperationMode.HEATING;
            }

            @Override
            public BigDecimal getValue() {
                return new BigDecimal(temp);
            }

            @Override
            public FanSpeed speed() {
                return speed;
            }
        };
    }
}
