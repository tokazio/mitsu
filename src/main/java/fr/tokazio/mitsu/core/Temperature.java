package fr.tokazio.mitsu.core;

import fr.tokazio.mitsu.mitsubishi.webapi.FanSpeed;
import fr.tokazio.mitsu.mitsubishi.webapi.OperationMode;

import java.math.BigDecimal;

public interface Temperature {

    Temperature AUTO = new Temperature() {
        @Override
        public OperationMode getMode() {
            return OperationMode.AUTO;
        }

        @Override
        public BigDecimal getValue() {
            return new BigDecimal(20);
        }

        @Override
        public FanSpeed speed() {
            return FanSpeed.AUTO;
        }
    };

    OperationMode getMode();

    BigDecimal getValue();

    FanSpeed speed();
}
