package fr.tokazio.mitsu.core;

public class StoppingException extends ProgrammeException {

    public StoppingException(final Exception ex) {
        super(ex);
    }
}
