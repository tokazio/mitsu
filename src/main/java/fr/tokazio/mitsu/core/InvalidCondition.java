package fr.tokazio.mitsu.core;

public class InvalidCondition extends Exception {


    public InvalidCondition(final Condition<?> cond) {
        super("Invalid condition " + cond.getMessage());
    }
}
