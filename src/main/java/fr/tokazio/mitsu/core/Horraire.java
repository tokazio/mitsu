package fr.tokazio.mitsu.core;

import java.util.Calendar;

public class Horraire {

    private final int h;
    private final int m;

    private Horraire(final int h, final int m) {
        this.h = h;
        this.m = m;
    }

    public static Horraire of(final int hours, final int minutes) {
        return new Horraire(hours, minutes);
    }

    public int hours() {
        return h;
    }

    public int minutes() {
        return m;
    }

    public boolean lowerOrEquals(final Calendar cal) {
        return cal.get(Calendar.HOUR_OF_DAY) > hours()
                || (cal.get(Calendar.HOUR_OF_DAY) == hours() && cal.get(Calendar.MINUTE) >= minutes());
    }

    public boolean greaterThan(final Calendar cal) {
        return cal.get(Calendar.HOUR_OF_DAY) < hours()
                || (cal.get(Calendar.HOUR_OF_DAY) == hours() && cal.get(Calendar.MINUTE) < minutes());
    }

    public String toString() {
        return h + ":" + m;
    }
}
