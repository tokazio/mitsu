package fr.tokazio.mitsu.core;

public class ConditionBuilder<T> {

    private final String message;

    public ConditionBuilder(final String message) {
        this.message = message;
    }

    public Condition<T> verifyWith(final Condition<T> in) {
        return new Condition<T>() {
            @Override
            public boolean verified(T data) {
                return in.verified(data);
            }

            @Override
            public String getMessage() {
                return message;
            }
        };
    }
}
