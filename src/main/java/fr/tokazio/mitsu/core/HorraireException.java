package fr.tokazio.mitsu.core;

public class HorraireException extends RuntimeException {

    public HorraireException(final String message) {
        super(message);
    }
}
