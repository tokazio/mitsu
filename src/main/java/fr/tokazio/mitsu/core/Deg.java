package fr.tokazio.mitsu.core;

import java.math.BigDecimal;

public class Deg {

    private Deg() {
        super();
    }

    public static BigDecimal of(final int temp) {
        return new BigDecimal(String.valueOf(temp));
    }

    public static BigDecimal of(final String temp) {
        return new BigDecimal(temp);
    }
}
