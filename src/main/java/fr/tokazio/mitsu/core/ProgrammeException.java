package fr.tokazio.mitsu.core;

public class ProgrammeException extends Exception {

    public ProgrammeException(final Exception ex) {
        super(ex);
    }
}
