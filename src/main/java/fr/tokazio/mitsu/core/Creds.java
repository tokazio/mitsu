package fr.tokazio.mitsu.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Creds {

    private static final Logger LOGGER = LoggerFactory.getLogger(Creds.class);

    private static final Properties prop = new Properties();

    static {
        try (InputStream input = new FileInputStream("creds.properties")) {
            prop.load(input);
        } catch (IOException e) {
            LOGGER.error("Error loading credentials from 'creds.properties. Gives 'user' and 'password' in 'creds.properties' file to be able to connect to MELCloud.");
        }
    }

    private Creds() {
        super();
    }

    public static String getUser() {
        return prop.getProperty("user");
    }

    public static String getPassword() {
        return prop.getProperty("password");
    }

    public static String get(final String key) {
        return prop.getProperty(key);
    }
}
