package fr.tokazio.mitsu.core;

public enum Power {

    ON(true),
    OFF(false);

    private final boolean bool;

    Power(final boolean bool) {
        this.bool = bool;
    }

    public boolean asBoolean() {
        return bool;
    }
}
