package fr.tokazio.mitsu.core;

import java.util.Calendar;

public class PlageHorraire {

    private final Horraire start;
    private final Horraire end;

    private PlageHorraire(final Horraire start, final Horraire end) {
        this.start = start;
        this.end = end;
    }

    public static PlageHorraire of(final Horraire start, final Horraire end) {
        return new PlageHorraire(start, end);
    }

    public static PlageHorraire of(final String start, final String end) {
        return of(parse(start), parse(end));
    }

    private static Horraire parse(final String str) {
        if (str.indexOf(':') < 0) {
            throw new HorraireException("Hours and minutes must be separated by ':'");
        }
        final String[] strs = str.split(":");
        return Horraire.of(Integer.parseInt(strs[0]), Integer.parseInt(strs[1]));
    }

    public boolean contains(final Calendar cal) {
        return start.lowerOrEquals(cal) && end.greaterThan(cal);
    }

    public String toString() {
        return start + " - " + end;
    }
}
