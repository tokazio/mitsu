package fr.tokazio.mitsu.core;

public class StartingException extends ProgrammeException {

    public StartingException(final Exception ex) {
        super(ex);
    }
}
