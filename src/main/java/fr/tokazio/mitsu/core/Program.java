package fr.tokazio.mitsu.core;

import java.util.Calendar;

public interface Program {

    void onStarting(ProgramVisitor visitor) throws StartingException;

    void onTic(ProgramVisitor visitor, Calendar maintenant) throws ProgrammeException;

    void onStopping(ProgramVisitor visitor) throws StoppingException;

    String getName();
}
