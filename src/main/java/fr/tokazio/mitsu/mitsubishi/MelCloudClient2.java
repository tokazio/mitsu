package fr.tokazio.mitsu.mitsubishi;

import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;
import fr.tokazio.mitsu.core.Creds;
import fr.tokazio.mitsu.core.Power;
import fr.tokazio.mitsu.core.ProgramVisitor;
import fr.tokazio.mitsu.core.Temperature;
import fr.tokazio.mitsu.mitsubishi.webapi.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class MelCloudClient2 implements ProgramVisitor {

    private static final Logger LOGGER = LoggerFactory.getLogger(MelCloudClient2.class);

    private final MelCloud melCloud;

    private String contextKey = "";
    private List<Device> devices;

    public MelCloudClient2() {
        melCloud = Feign.builder()
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .client(new OkHttpClient())
                .logger(new Slf4jLogger())
                .logLevel(feign.Logger.Level.FULL)
                .target(MelCloud.class, MelCloud.MEL_CLOUD_ROOT_URL);
        login();
        //LOGGER.info(getDevices().toString());
    }

    private MelCloudClient2 login() {
        final LoginResp resp = melCloud.login(new Login(Creds.getUser(), Creds.getPassword()));
        contextKey = resp.getLoginData().getContextKey();
        LOGGER.debug("App context key from web: {}", contextKey);
        return this;
    }

    private List<ListResp> listDevices() {
        return melCloud.listDevices(contextKey);
    }

    public State state(final int deviceId, final int buildingId) {
        return melCloud.state(contextKey, deviceId, buildingId);
    }

    public List<Device> getDevices() {
        if (devices == null) {
            devices = listDevices().stream().map(ListResp::getDevices).findAny().orElse(Collections.EMPTY_LIST);
        }
        return devices;
    }

    public State power(final Device device, final Power onOrOff, final Temperature temp) throws IOException {
        final State state = state(device.getBuildingId(), device.getId());

        /*
        EffectiveFlags values selon la commande voulue:
        Power : 1
        OperationMode : 2
        SetTemperature : 4
        SetFanSpeed : 8

        Si la propriété OperationMode est modifiée, il va faloir incrémenter EffectiveFlags de 2.
        Si les propriétés OperationMode et SetTemperature sont modifiées, il va faloir incrémenter EffectiveFlags de 2 + 4, 6.


        HasPendingCommand doit être true
        */
        state.hasPendingCommand(true);
        state.power(onOrOff.asBoolean());
        if (onOrOff.asBoolean()) {
            state.setEffectiveFlags(15);//all params changes
            state.setTemperature(temp.getValue());
            state.setFanSpeed(temp.speed().asInt());
            state.setOperationMode(temp.getMode().asInt());
        } else {
            state.setEffectiveFlags(1);//only power changes
        }

        return melCloud.set(contextKey, state);
    }

    public Device find(final String deviceName) throws DeviceNotFoundException {
        for (Device device : devices) {
            if (device.getName().equals(deviceName)) {
                return device;
            }
        }
        throw new DeviceNotFoundException("Device " + deviceName + " NOT FOUND");
    }
}
