package fr.tokazio.mitsu.mitsubishi.api;

public class ClimException extends RuntimeException {

    public ClimException(final Exception e) {
        super(e);
    }
}
