package fr.tokazio.mitsu.mitsubishi.api;


import fr.tokazio.mitsu.core.CoreDevice;
import fr.tokazio.mitsu.core.Power;
import fr.tokazio.mitsu.core.Temperature;
import fr.tokazio.mitsu.mitsubishi.MelCloudClient2;
import fr.tokazio.mitsu.mitsubishi.webapi.Device;
import fr.tokazio.mitsu.mitsubishi.webapi.State;

import java.io.IOException;
import java.math.BigDecimal;

public class Clim implements CoreDevice {

    private final Temperature askedTemperature;
    private final Device device;
    private final MelCloudClient2 client;

    private Clim(final MelCloudClient2 client, final Device device, final Temperature temp) {
        this.client = client;
        this.device = device;
        this.askedTemperature = temp;
    }

    public static Clim of(final MelCloudClient2 client, final Device device) {
        return new Clim(client, device, Temperature.AUTO);
    }

    public static Clim of(final MelCloudClient2 client, final Device device, final Temperature temp) {
        return new Clim(client, device, temp);
    }

    public String getName() {
        return device.getName();
    }

    private State getState() {
        return client.state(device.getId(), device.getBuildingId());
    }

    public boolean isPowered() {
        return getState().isPowered();
    }

    public boolean isNotPowered() {
        return !isPowered();
    }

    public BigDecimal getRoomTemperature() {
        return getState().getRoomTemperature();
    }

    public BigDecimal getExternalTemperature() {
        return getState().getExternalTemperateure();
    }

    public boolean hasRoomTemperatureUnder(final String temp) {
        return getRoomTemperature() != null && getRoomTemperature().compareTo(new BigDecimal(temp)) < 0;
    }

    public boolean hasExternalTemperatureHigherThan(String temp) {
        return getExternalTemperature() != null && getExternalTemperature().compareTo(new BigDecimal(temp)) > 0;
    }

    public Clim powerOn() {
        try {
            client.power(device, Power.ON, Temperature.AUTO);
        } catch (IOException e) {
            throw new ClimException(e);
        }
        return this;
    }

    public void powerOn(final Temperature temp) {
        try {
            client.power(device, Power.ON, temp);
        } catch (IOException e) {
            throw new ClimException(e);
        }
    }

    public void powerOff() {
        try {
            client.power(device, Power.OFF, Temperature.AUTO);//Temperature will not be really used here
        } catch (IOException e) {
            throw new ClimException(e);
        }
    }

    public Temperature getAskedTemperature() {
        return askedTemperature;
    }

    @Override
    public String toString() {
        return device.getName();
    }
}
