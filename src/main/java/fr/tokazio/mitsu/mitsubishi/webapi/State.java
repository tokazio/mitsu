package fr.tokazio.mitsu.mitsubishi.webapi;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class State {

    @JsonProperty("ErrorMessage")
    private String errorMessage;
    @JsonProperty("ErrorCode")
    private int errorCode;
    @JsonProperty("DefaultHeatingSetTemperature")
    private BigDecimal defaultHeatingSetTemperature;
    @JsonProperty("DefaultCoolingSetTemperature")
    private BigDecimal defaultCoolingSetTemperature;
    @JsonProperty("HideVaneControls")
    private boolean hideVaneControls;
    @JsonProperty("HideDryModeControl")
    private boolean hideDryModeControl;
    @JsonProperty("RoomTemperatureLabel")
    private int roomTemperatureLabel;
    @JsonProperty("InStandbyMode")
    private boolean inStandbyMode;
    @JsonProperty("TemperatureIncrementOverride")
    private int temperatureIncrementOverride;
    @JsonProperty("ProhibitSetTemperature")
    private boolean prohibitSetTemperature;
    @JsonProperty("ProhibitOperationMode")
    private boolean prohibitOperationMode;
    @JsonProperty("ProhibitPower")
    private boolean prohibitPower;
    @JsonProperty("DeviceID")
    private int deviceID;
    @JsonProperty("DeviceType")
    private int deviceType;
    @JsonProperty("LastCommunication")
    private String lastCommunication;//2021-11-07T09:36:50.82
    @JsonProperty("NextCommunication")
    private String nextCommunication;//2021-11-07T09:37:50.82
    @JsonProperty("Power")
    private boolean power;
    @JsonProperty("HasPendingCommand")
    private boolean hasPendingCommand;
    @JsonProperty("Offline")
    private boolean offline;
    @JsonProperty("Scene")
    private String scene;
    @JsonProperty("SceneOwner")
    private String sceneOwner;
    @JsonProperty("EffectiveFlags")
    private int effectiveFlags;
    @JsonProperty("LocalIPAddress")
    private String localIPAddress;
    @JsonProperty("RoomTemperature")
    private BigDecimal roomTemperature;
    @JsonProperty("SetTemperature")
    private BigDecimal setTemperature;
    @JsonProperty("SetFanSpeed")
    private int setFanSpeed;
    @JsonProperty("OperationMode")
    private int operationMode;
    @JsonProperty("VaneHorizontal")
    private int vaneHorizontal;
    @JsonProperty("VaneVertical")
    private int vaneVertical;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("NumberOfFanSpeeds")
    private int numberOfFanSpeeds;
    @JsonProperty("WeatherObservations")
    private List<WeatherObservation> weatherObservations;

    @Override
    public String toString() {
        return "State{" +
                "effectiveFlags=" + effectiveFlags +
                ", localIPAddress='" + localIPAddress + '\'' +
                ", roomTemperature=" + roomTemperature +
                ", setTemperature=" + setTemperature +
                ", setFanSpeed=" + setFanSpeed +
                ", operationMode=" + operationMode +
                ", vaneHorizontal=" + vaneHorizontal +
                ", vaneVertical=" + vaneVertical +
                ", name='" + name + '\'' +
                ", numberOfFanSpeeds=" + numberOfFanSpeeds +
                ", weatherObservations=" + weatherObservations +
                '}';
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public int getDeviceID() {
        return deviceID;
    }

    public boolean isPowered() {
        return power;
    }

    public boolean isOffline() {
        return offline;
    }

    public int getEffectiveFlags() {
        return effectiveFlags;
    }

    public BigDecimal getRoomTemperature() {
        return roomTemperature;
    }

    public BigDecimal getSetTemperature() {
        return setTemperature;
    }

    public int getVaneHorizontal() {
        return vaneHorizontal;
    }

    public int getVaneVertical() {
        return vaneVertical;
    }

    public BigDecimal getExternalTemperateure() {
        return weatherObservations.get(0).temperature;
    }

    public void power(boolean b) {
        this.power = b;
    }

    public State externalTemperature(final BigDecimal temp) {
        weatherObservations = new ArrayList<>(1);
        final WeatherObservation wo = new WeatherObservation();
        wo.temperature = temp;
        weatherObservations.add(wo);
        return this;
    }

    public void roomTemperature(BigDecimal temp) {
        this.roomTemperature = temp;
    }

    public State hasPendingCommand(boolean b) {
        hasPendingCommand = b;
        return this;
    }

    public State setEffectiveFlags(int i) {
        effectiveFlags = i;
        return this;
    }

    public State setTemperature(BigDecimal temp) {
        setTemperature = temp;
        return this;
    }

    public State setFanSpeed(int asInt) {
        this.setFanSpeed = asInt;
        return this;
    }

    public State setOperationMode(int asInt) {
        this.operationMode = asInt;
        return this;
    }

    public State setRoomTemperature(BigDecimal temp) {
        this.roomTemperature = temp;
        return this;
    }

    public State setWeatherObservations(List<WeatherObservation> aList) {
        weatherObservations=aList;
        return this;
    }

    public State setPower(boolean b) {
        this.power = b;
        return this;
    }
}
