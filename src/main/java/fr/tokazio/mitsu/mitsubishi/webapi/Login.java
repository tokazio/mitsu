package fr.tokazio.mitsu.mitsubishi.webapi;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Login {

    @JsonProperty("Email")
    String email;
    @JsonProperty("Password")
    String password;
    @JsonProperty("Language")
    int language = 7;
    @JsonProperty("AppVersion")
    String appVersion = "1.22.7.0";
    @JsonProperty("Persist")
    boolean persist = true;
    @JsonProperty("CaptchaResponse")
    String CaptchaResponse = null;

    public Login(final String email, final String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getLanguage() {
        return language;
    }

    public void setLanguage(int language) {
        this.language = language;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public boolean isPersist() {
        return persist;
    }

    public void setPersist(boolean persist) {
        this.persist = persist;
    }

    public String getCaptchaResponse() {
        return CaptchaResponse;
    }

    public void setCaptchaResponse(String captchaResponse) {
        CaptchaResponse = captchaResponse;
    }

    @Override
    public String toString() {
        return "Login{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", language=" + language +
                ", appVersion='" + appVersion + '\'' +
                ", persist=" + persist +
                ", CaptchaResponse='" + CaptchaResponse + '\'' +
                '}';
    }
}
