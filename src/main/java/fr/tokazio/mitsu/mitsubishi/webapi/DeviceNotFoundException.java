package fr.tokazio.mitsu.mitsubishi.webapi;

public class DeviceNotFoundException extends Exception {

    public DeviceNotFoundException(final String s) {
        super(s);
    }
}
