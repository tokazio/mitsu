package fr.tokazio.mitsu.mitsubishi.webapi;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginResp {

    @JsonProperty("LoginData")
    private LoginData loginData;

    public LoginData getLoginData() {
        return loginData;
    }

    public void setLoginData(final LoginData loginData) {
        this.loginData = loginData;
    }

    @Override
    public String toString() {
        return "LoginResp{" +
                "loginData=" + loginData +
                '}';
    }
}
