package fr.tokazio.mitsu.mitsubishi.webapi;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class WeatherObservation {

    @JsonProperty("Date")
    String date;//2021-11-07T09:00:00
    @JsonProperty("Sunrise")
    String sunrise;
    @JsonProperty("Sunset")
    String sunset;
    @JsonProperty("Condition")
    int condition;
    @JsonProperty("ID")
    int id;
    @JsonProperty("Humidity")
    BigDecimal humidity;
    @JsonProperty("Temperature")
    BigDecimal temperature;
    @JsonProperty("Icon")
    String icon;
    @JsonProperty("ConditionName")
    String conditionName;
    @JsonProperty("Day")
    int day;
    @JsonProperty("WeatherType")
    int weatherType;


    @Override
    public String toString() {
        return "WeatherObservation{" +
                "date='" + date + '\'' +
                ", sunrise='" + sunrise + '\'' +
                ", sunset='" + sunset + '\'' +
                ", condition=" + condition +
                ", id=" + id +
                ", humidity=" + humidity +
                ", temperature=" + temperature +
                ", icon='" + icon + '\'' +
                ", conditionName='" + conditionName + '\'' +
                ", day=" + day +
                ", weatherType=" + weatherType +
                '}';
    }

    public WeatherObservation setTemperature(final BigDecimal temp) {
        this.temperature = temp;
        return this;
    }
}
