package fr.tokazio.mitsu.mitsubishi.webapi;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class Structure {

    @JsonProperty("Floors")
    List<Floor> floors;

    @Override
    public String toString() {
        return "Structure{" +
                "floors=" + floors +
                '}';
    }

    public List<Floor> getFloors() {
        return floors;
    }

    public List<Device> getDevices() {
        final List<Device> out = new ArrayList<>();
        for (Floor floor : floors) {
            out.addAll(floor.getDevices());
        }
        return out;
    }
}
