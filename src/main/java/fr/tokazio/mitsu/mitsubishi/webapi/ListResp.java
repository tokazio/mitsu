package fr.tokazio.mitsu.mitsubishi.webapi;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.stream.Collectors;

public class ListResp {

    @JsonProperty("ID")
    int id;

    @JsonProperty("Name")
    String name;

    @JsonProperty("Structure")
    Structure structure;

    @Override
    public String toString() {
        return "ListResp{" +
                "name='" + name + '\'' +
                ", structure=" + structure +
                '}';
    }

    public int getId() {
        return id;
    }

    public Structure getStructure() {
        return structure;
    }

    public List<Device> getDevices() {

        return getStructure().getDevices().stream()
                .peek(device -> {
                    device.setBuildingId(id);
                }).collect(Collectors.toList());
    }
}
