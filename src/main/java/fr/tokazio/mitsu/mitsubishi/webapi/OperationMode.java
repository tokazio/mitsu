package fr.tokazio.mitsu.mitsubishi.webapi;

public enum OperationMode {

    HEATING(1),
    COLDING(3),
    VENTIL(7),
    AUTO(8);

    private final int mode;

    OperationMode(final int mode) {
        this.mode = mode;
    }

    public int asInt() {
        return mode;
    }
}
