package fr.tokazio.mitsu.mitsubishi.webapi;

import feign.Headers;
import feign.Param;
import feign.RequestLine;

import java.util.List;

@Headers({
//        "application/json, text/javascript, */*; q=0.01",
        "accept-encoding: gzip, deflate, br",
        "accept-language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7",
        "cache-control: no-cache",
        "content-type: application/json; charset=UTF-8",
        "X-MitsContextKey: {contextKey}"
})
public interface MelCloud {

        String MEL_CLOUD_ROOT_URL = "https://app.melcloud.com/Mitsubishi.Wifi.Client";

        @RequestLine("GET /User/ListDevices")
        List<ListResp> listDevices(@Param("contextKey") String contextKey);

        @RequestLine("POST /Login/ClientLogin")
        LoginResp login(Login login);

        @RequestLine("GET /Device/Get?id={deviceId}&buildingId={buildingId}")
        State state(@Param("contextKey") String contextKey, @Param("deviceId") int deviceId, @Param("buildingId") int buildingId);

        @RequestLine("POST /Device/SetAta")
        State set(@Param("contextKey") String contextKey, State state);

}
