package fr.tokazio.mitsu.mitsubishi.webapi;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Device {

    @JsonIgnore
    private int buildingId = -1;

    @JsonProperty("DeviceID")
    private int deviceId;

    @JsonProperty("DeviceName")
    private String deviceName;

    @JsonProperty("Device")
    private DeviceDetail detail;

    public int getId() {
        return deviceId;
    }

    public String getName() {
        return deviceName;
    }

    public DeviceDetail getDetails() {
        return detail;
    }

    @Override
    public String toString() {
        return "Device{" +
                "deviceId=" + deviceId +
                ", deviceName='" + deviceName + '\'' +
                ", device=" + detail +
                '}';
    }

    public int getBuildingId() {
        return buildingId;
    }

    public Device setBuildingId(final int buildingId) {
        this.buildingId = buildingId;
        return this;
    }
}
