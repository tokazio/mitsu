package fr.tokazio.mitsu.mitsubishi.webapi;

public enum FanSpeed {

    AUTO(0),
    SLOW(1),
    NORMAL(2),
    HIGH(3);

    private final int speed;

    FanSpeed(final int speed) {
        this.speed = speed;
    }

    public int asInt() {
        return speed;
    }
}
