package fr.tokazio.mitsu.mitsubishi.webapi;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class DeviceDetail {

    @JsonProperty("RoomTemperature")
    BigDecimal roomTemperature;

    @JsonProperty("SetTemperature")
    BigDecimal setTemperature;

    @Override
    public String toString() {
        return "DeviceDetail{" +
                "roomTemperature=" + roomTemperature +
                ", setTemperature=" + setTemperature +
                '}';
    }
}
