package fr.tokazio.mitsu.mitsubishi.webapi;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Floor {

    @JsonProperty("Name")
    String name;

    @JsonProperty("Devices")
    List<Device> devices;

    @Override
    public String toString() {
        return "Floor{" +
                "name='" + name + '\'' +
                ", devices=" + devices +
                '}';
    }

    public List<Device> getDevices() {
        return devices;
    }
}
