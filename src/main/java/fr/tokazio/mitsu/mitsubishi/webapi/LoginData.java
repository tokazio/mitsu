package fr.tokazio.mitsu.mitsubishi.webapi;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginData {

    @JsonProperty("ContextKey")
    private String contextKey;

    public String getContextKey() {
        return contextKey;
    }

    public void setContextKey(final String contextKey) {
        this.contextKey = contextKey;
    }

    @Override
    public String toString() {
        return "LoginData{" +
                "ContextKey='" + contextKey + '\'' +
                '}';
    }
}
