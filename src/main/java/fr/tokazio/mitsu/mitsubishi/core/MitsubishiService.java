package fr.tokazio.mitsu.mitsubishi.core;

import fr.tokazio.mitsu.core.*;
import fr.tokazio.mitsu.mitsubishi.MelCloudClient2;
import fr.tokazio.mitsu.mitsubishi.api.Clim;
import fr.tokazio.mitsu.mitsubishi.webapi.Device;
import fr.tokazio.mitsu.mitsubishi.webapi.DeviceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class MitsubishiService implements Service, ProgramVisitor {

    private static final Logger LOGGER = LoggerFactory.getLogger(MitsubishiService.class);
    private static final long SLEEPING_MS = 1000L * 60 * 5;//5 minutes

    private final SimpleDateFormat dateFmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    private final AtomicBoolean interrupted = new AtomicBoolean(false);
    private final MelCloudClient2 client;
    private final List<Program> programList = new ArrayList<>();

    public MitsubishiService() {
        client = new MelCloudClient2();
    }

    public MitsubishiService addProgramme(Program pg) {
        if (pg != null) {
            programList.add(pg);
        }
        return this;
    }

    public void start() throws InterruptedException {
        for (Program p : programList) {
            try {
                LOGGER.info("Starting program '{}' ...", p.getName());
                p.onStarting(this);
            } catch (StartingException e) {
                LOGGER.error("Error on starting program " + p.getClass().getName(), e);
            }
        }

        while (!interrupted.get()) {
            final Calendar now = Calendar.getInstance();
            if (LOGGER.isInfoEnabled()) {
                LOGGER.info("Il est {}", dateFmt.format(now.getTime()));
            }
            for (Program p : programList) {
                try {
                    LOGGER.info("Executing program '{}' ...", p.getName());
                    p.onTic(this, now);
                } catch (ProgrammeException e) {
                    LOGGER.error("Error on running 'tic' from program " + p.getClass().getName(), e);
                }
            }
            Thread.sleep(SLEEPING_MS);
        }

        for (Program p : programList) {
            try {
                LOGGER.info("Stopping program '{}' ...", p.getName());
                p.onStopping(this);
            } catch (StoppingException e) {
                LOGGER.error("Error on stopping program " + p.getClass().getName(), e);
            }
        }
    }

    private Device find(final String deviceName) throws DeviceNotFoundException {
        for (Device device : client.getDevices()) {
            if (device.getName().equals(deviceName)) {
                return device;
            }
        }
        throw new DeviceNotFoundException("Device " + deviceName + " not found");
    }

    public Clim climFrom(final String deviceName) {
        try {
            return Clim.of(client, find(deviceName));
        } catch (DeviceNotFoundException e) {
            LOGGER.error("Device '{}' not found", deviceName, e);
        }
        return null;
    }
}
