package fr.tokazio.mitsu;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Data {

    private final Date date;
    private final String device;
    private final BigDecimal roomTemp;
    private final BigDecimal extTemp;
    private final boolean powered;

    public Data(final String row) {
        //2021-11-11-04-35-09	Chambre Nous	17.5	2
        final String[] strs = row.split("\t");
        Date date1;
        try {
            SimpleDateFormat dateFmt = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
            date1 = dateFmt.parse(strs[0]);
        } catch (ParseException e) {
            date1 = new Date(0);
        }
        date = date1;
        device = strs[1];
        roomTemp = new BigDecimal(strs[2]);
        if (strs.length > 3) {
            extTemp = new BigDecimal(strs[3]);
        } else {
            extTemp = BigDecimal.ZERO;
        }
        if (strs.length > 4) {
            powered = "true".equals(strs[4]);
        } else {
            powered = false;
        }
    }

    public String getName() {
        return device;
    }

    public BigDecimal getRoomTemp() {
        return roomTemp;
    }

    public Date getDate() {
        return date;
    }

    public BigDecimal getExternalTemp() {
        return extTemp;
    }

    public boolean isPowered() {
        return powered;
    }
}
