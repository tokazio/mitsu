package fr.tokazio.mitsu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileAppender {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileAppender.class);
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");

    private FileAppender() {
        super();
    }

    public static void appendTo(final String filename, final String row) {
        try (BufferedWriter output = new BufferedWriter(new FileWriter(filename, true))) {
            output.append(row).append("\n");
        } catch (IOException e) {
            LOGGER.error("Error appending '{}' to file {}", row, filename);
        }
    }

    public static void timedAppendTo(final String filename, final String row) {
        appendTo(filename, timed() + "\t" + row);
    }

    private static String timed() {
        return DATE_FORMAT.format(new Date());
    }

}
