package fr.tokazio.mitsu;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class FileReader {

    private FileReader() {
        super();
    }

    public static void read(final String filename, final Consumer<String> consumer) throws IOException {
        try (Stream<String> stream = Files.lines(Path.of(filename))) {
            stream.forEach(consumer);
        }
    }

}
