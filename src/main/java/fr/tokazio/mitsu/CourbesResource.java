package fr.tokazio.mitsu;

import fr.tokazio.mitsu.server.Handler;
import fr.tokazio.mitsu.server.HandlerException;
import fr.tokazio.mitsu.server.Request;
import fr.tokazio.mitsu.server.Response;

import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.stream.Stream;

public class CourbesResource implements Handler {

    private final SimpleDateFormat dateFmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    private final String selection;

    public CourbesResource(final String selection) {
        this.selection = selection;
    }

    private void header(final StringBuilder sb) {
        sb.append("<html>\n" +
                "  <head>\n" +
                "    <script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>\n" +
                "    <script type=\"text/javascript\">\n" +
                "      google.charts.load('current', {'packages':['corechart']});\n" +
                "      google.charts.setOnLoadCallback(drawChart);\n" +
                "\n" +
                "      function drawChart() {\n" +
                "        var data = google.visualization.arrayToDataTable([\n");
    }


    private void footer(final StringBuilder sb) {
        sb.append("        ]);\n" +
                "\n" +
                "        var options = {\n" +
                "          title: 'Temperatures',\n" +
                "          curveType: 'function',\n" +
                "          legend: { position: 'bottom' }\n" +
                "        };\n" +
                "\n" +
                "        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));\n" +
                "\n" +
                "        chart.draw(data, options);\n" +
                "      }\n" +
                "    </script>\n" +
                "  </head>\n" +
                "  <body>\n" +
                "    <form>" +
                "    </form>" +
                "    <div id=\"curve_chart\" style=\"width: 900px; height: 500px\"></div>\n" +
                "  </body>\n" +
                "</html>");
    }

    @Override
    public Object handle(Request request, Response response) throws HandlerException {

        //  System.out.println(request.uri());
        //  System.out.println(request.parameter( "from"));

        final StringBuilder responseData = new StringBuilder();
        header(responseData);
        responseData.append("['date','Pièce','Dehors','Powered'],\n");
        try (Stream<String> stream = Files.lines(Path.of("temps.csv"))) {
            stream
                    .map(Data::new)
                    .filter(data -> selection.equals(data.getName()))
                    .forEach(data ->
                            responseData
                                    .append("['").append(dateFmt.format(data.getDate())).append("',")
                                    .append(data.getRoomTemp()).append(",")
                                    .append(data.getExternalTemp()).append(",")
                                    .append(data.isPowered() ? 20 : 0)
                                    .append("],\n")
                    );
        } catch (Exception e) {
            throw new HandlerException(e);
        }
        footer(responseData);
        return responseData.toString();
    }

    @Override
    public String getProduces() {
        return "text/html";
    }
}
