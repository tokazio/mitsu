package fr.tokazio.mitsu.server;

import io.netty.handler.codec.http.HttpMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RouteTable {

    private static final Logger LOGGER = LoggerFactory.getLogger(RouteTable.class);

    private final List<Route> routes = new ArrayList<>();

    public void addRoute(final Route route) {
        LOGGER.debug("Adding route {}", route.getPath());
        this.routes.add(route);
        Collections.sort(routes);
    }

    public Route findRoute(final HttpMethod method, final String path) {
        //TODO check ? in route.matches
        final int params = path.indexOf('?');
        String r = path;
        if (params >= 0) {
            r = path.substring(0, params);
        }
        LOGGER.debug("Searching route for '{}'...", r);
        for (final Route route : routes) {
            if (route.matches(method, r)) {
                return route;
            }
        }

        return null;//TODO throw not found
    }
}