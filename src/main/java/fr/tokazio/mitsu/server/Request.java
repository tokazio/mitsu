package fr.tokazio.mitsu.server;

import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.QueryStringDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;

/**
 * The Request class provides convenience helpers to the underyling HTTP Request.
 */
public class Request {

    private static final Logger LOGGER = LoggerFactory.getLogger(Request.class);

    private final FullHttpRequest fullRequest;

    public Request(final FullHttpRequest request) {
        this.fullRequest = request;
    }

    public String body() {
        return fullRequest.content().toString(StandardCharsets.UTF_8);
    }

    public String uri() {
        return fullRequest.uri();
    }

    public String parameter(final String paramName) {
        QueryStringDecoder decoder = new QueryStringDecoder(fullRequest.uri());
        return decoder.parameters().get(paramName).get(0);
    }

}