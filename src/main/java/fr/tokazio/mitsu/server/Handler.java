package fr.tokazio.mitsu.server;

public interface Handler {

    Object handle(Request request, Response response) throws HandlerException;

    String getProduces();
}