package fr.tokazio.mitsu.server;

public interface Parser<T> {

    T parse(String s);
}
