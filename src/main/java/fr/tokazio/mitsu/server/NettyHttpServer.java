package fr.tokazio.mitsu.server;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.kqueue.KQueue;
import io.netty.channel.kqueue.KQueueEventLoopGroup;
import io.netty.channel.kqueue.KQueueServerSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.ServerSocketChannel;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Using Netty to implements an HTTP server.
 * Using EPoll or KQueue when possible, if not, using nio.
 * <p>
 * new NettyHttpServer("server-name",80)
 * .get("/getEndPoint", new ExampleGetHandlerImpl())
 * .post("/postEndPoint", new ExamplePostHandlerImpl())
 * .startAsync();
 */
public class NettyHttpServer {

    public static final String TYPE_PLAIN = "text/plain; charset=UTF-8";
    public static final String TYPE_JSON = "application/json; charset=UTF-8";
    private static final Logger LOGGER = LoggerFactory.getLogger(NettyHttpServer.class);
    private static final ObjectMapper mapper = new ObjectMapper();
    private final RouteTable routeTable = new RouteTable();
    private final EventLoopGroup masterGroup;
    private final EventLoopGroup slaveGroup;
    private final int port;
    private final String serverName;
    private ChannelFuture ch;
    private Channel ch2;

    public NettyHttpServer(final String serverName, final int port) {
        this.serverName = serverName != null ? serverName : "no-server-name";
        this.port = port;
        if (Epoll.isAvailable()) {
            LOGGER.info("Serving using Epoll @{}", port);
            masterGroup = new EpollEventLoopGroup();
            slaveGroup = new EpollEventLoopGroup();
        } else if (KQueue.isAvailable()) {
            LOGGER.info("Serving using Kqueue @{}", port);
            masterGroup = new KQueueEventLoopGroup();
            slaveGroup = new KQueueEventLoopGroup();
        } else {
            LOGGER.info("Serving using nio @{}", port);
            masterGroup = new NioEventLoopGroup();
            slaveGroup = new NioEventLoopGroup();
        }
    }

    public void stop() {
        if (ch2 == null) {
            ch2 = ch.channel();
        }
        ch2.close();
        slaveGroup.shutdownGracefully();
        masterGroup.shutdownGracefully();
    }

    public void start() {
        startAsync();
        try {
            ch2 = ch.sync().channel();
            ch2.closeFuture().sync();
        } catch (InterruptedException e) {
            LOGGER.error("Error starting HTTP server", e);
            Thread.currentThread().interrupt();
        } finally {
            stop();
        }
    }

    private Class<? extends ServerSocketChannel> channel() {
        if (Epoll.isAvailable()) {
            return EpollServerSocketChannel.class;
        }
        if (KQueue.isAvailable()) {
            return KQueueServerSocketChannel.class;
        }
        return NioServerSocketChannel.class;
    }

    public void startAsync() {
        final ServerBootstrap bootstrap =
                new ServerBootstrap()
                        .group(masterGroup, slaveGroup)
                        .channel(channel())
                        .childHandler(new WebServerInitializer())
                        .option(ChannelOption.SO_BACKLOG, 128)
                        .option(ChannelOption.SO_REUSEADDR, true)
                        .childOption(ChannelOption.ALLOCATOR, new PooledByteBufAllocator(true))
                        .childOption(ChannelOption.SO_REUSEADDR, true)
                        .childOption(ChannelOption.SO_KEEPALIVE, true);
        ch = bootstrap.bind(port);
    }

    /**
     * Adds a GET route.
     *
     * @param path    The URL path.
     * @param handler The request handler.
     * @return This NettyHttpServer.
     */
    public NettyHttpServer get(final String path, final Handler handler) {
        this.routeTable.addRoute(new Route(HttpMethod.GET, path, handler));
        return this;
    }

    /**
     * Adds a POST route.
     *
     * @param path    The URL path.
     * @param handler The request handler.
     * @return This NettyHttpServer.
     */
    public NettyHttpServer post(final String path, final Handler handler) {
        this.routeTable.addRoute(new Route(HttpMethod.POST, path, handler));
        return this;
    }

    /**
     * The Initializer class initializes the HTTP channel.
     */
    private class WebServerInitializer extends ChannelInitializer<SocketChannel> {

        /**
         * Initializes the channel pipeline with the HTTP response handlers.
         *
         * @param ch The Channel which was registered.
         */
        @Override
        public void initChannel(SocketChannel ch) {
            final ChannelPipeline p = ch.pipeline();
            p.addLast("decoder", new HttpRequestDecoder(4096, 8192, 8192, false));
            p.addLast("aggregator", new HttpObjectAggregator(100 * 1024 * 1024));
            p.addLast("encoder", new HttpResponseEncoder());
            p.addLast("handler", new WebServerHandler());
        }
    }

    /**
     * The Handler class handles all inbound channel messages.
     */
    private class WebServerHandler extends SimpleChannelInboundHandler<Object> {

        /**
         * Handles a new message.
         *
         * @param ctx The channel context.
         * @param msg The HTTP request message.
         */
        @Override
        public void messageReceived(final ChannelHandlerContext ctx, final Object msg) {
            if (!(msg instanceof FullHttpRequest)) {
                return;
            }

            final FullHttpRequest request = (FullHttpRequest) msg;

            if (HttpHeaderUtil.is100ContinueExpected(request)) {
                send100Continue(ctx);
            }

            final HttpMethod method = request.method();
            final String uri = request.uri();

            final Route route = NettyHttpServer.this.routeTable.findRoute(method, uri);
            if (route == null) {
                writeNotFound(ctx, request);
                return;
            }

            try {
                final Request requestWrapper = new Request(request);
                final Handler handler = route.getHandler();
                final Object obj = handler.handle(requestWrapper, null);
                final String content = obj == null ? "" : obj.toString();
                writeResponse(ctx, request, HttpResponseStatus.OK, handler.getProduces(), content);
            } catch (final Exception ex) {
                ex.printStackTrace();
                writeInternalServerError(ctx, request, ex);
            }
        }

        /**
         * Handles an exception caught.  Closes the context.
         *
         * @param ctx   The channel context.
         * @param cause The exception.
         */
        @Override
        public void exceptionCaught(final ChannelHandlerContext ctx, final Throwable cause) {
            ctx.close();
        }

        /**
         * Handles read complete event.  Flushes the context.
         *
         * @param ctx The channel context.
         */
        @Override
        public void channelReadComplete(final ChannelHandlerContext ctx) {
            ctx.flush();
        }

        /**
         * Writes a 404 Not Found response.
         *
         * @param ctx     The channel context.
         * @param request The HTTP request.
         */
        private void writeNotFound(
                final ChannelHandlerContext ctx,
                final FullHttpRequest request) {

            writeErrorResponse(ctx, request, HttpResponseStatus.NOT_FOUND);
        }

        /**
         * Writes a 500 Internal Server Error response.
         *
         * @param ctx     The channel context.
         * @param request The HTTP request.
         */
        private void writeInternalServerError(
                final ChannelHandlerContext ctx,
                final FullHttpRequest request, Exception ex) {

            try {
                writeResponse(ctx, request, HttpResponseStatus.INTERNAL_SERVER_ERROR, TYPE_PLAIN, mapper.writeValueAsString(new ExceptionRest(ex)));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                writeErrorResponse(ctx, request, HttpResponseStatus.INTERNAL_SERVER_ERROR);
            }
        }

        /**
         * Writes a HTTP error response.
         *
         * @param ctx     The channel context.
         * @param request The HTTP request.
         * @param status  The error status.
         */
        private void writeErrorResponse(
                final ChannelHandlerContext ctx,
                final FullHttpRequest request,
                final HttpResponseStatus status) {

            writeResponse(ctx, request, status, TYPE_PLAIN, status.reasonPhrase().toString());
        }

        /**
         * Writes a HTTP response.
         *
         * @param ctx         The channel context.
         * @param request     The HTTP request.
         * @param status      The HTTP status code.
         * @param contentType The response content type.
         * @param content     The response content.
         */
        private void writeResponse(
                final ChannelHandlerContext ctx,
                final FullHttpRequest request,
                final HttpResponseStatus status,
                final CharSequence contentType,
                final String content) {

            final byte[] bytes = content.getBytes(StandardCharsets.UTF_8);
            final ByteBuf entity = Unpooled.wrappedBuffer(bytes);
            writeResponse(ctx, request, status, entity, contentType, bytes.length);
        }

        /**
         * Writes a HTTP response.
         *
         * @param ctx           The channel context.
         * @param request       The HTTP request.
         * @param status        The HTTP status code.
         * @param buf           The response content buffer.
         * @param contentType   The response content type.
         * @param contentLength The response content length;
         */
        private void writeResponse(
                final ChannelHandlerContext ctx,
                final FullHttpRequest request,
                final HttpResponseStatus status,
                final ByteBuf buf,
                final CharSequence contentType,
                final int contentLength) {

            // Decide whether to close the connection or not.
            final boolean keepAlive = HttpHeaderUtil.isKeepAlive(request);

            // Build the response object.
            final FullHttpResponse response = new DefaultFullHttpResponse(
                    HttpVersion.HTTP_1_1,
                    status,
                    buf,
                    false);

            final ZonedDateTime dateTime = ZonedDateTime.now();
            final DateTimeFormatter formatter = DateTimeFormatter.RFC_1123_DATE_TIME;

            final DefaultHttpHeaders headers = (DefaultHttpHeaders) response.headers();
            headers.set(HttpHeaderNames.SERVER, serverName);
            headers.set(HttpHeaderNames.DATE, dateTime.format(formatter));
            headers.set(HttpHeaderNames.CONTENT_TYPE, contentType);
            headers.set(HttpHeaderNames.CONTENT_LENGTH, Integer.toString(contentLength));

            // Close the non-keep-alive connection after the write operation is done.
            if (!keepAlive) {
                ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
            } else {
                ctx.writeAndFlush(response, ctx.voidPromise());
            }
        }

        /**
         * Writes a 100 Continue response.
         *
         * @param ctx The HTTP handler context.
         */
        private void send100Continue(final ChannelHandlerContext ctx) {
            ctx.write(new DefaultFullHttpResponse(
                    HttpVersion.HTTP_1_1,
                    HttpResponseStatus.CONTINUE));
        }
    }

}