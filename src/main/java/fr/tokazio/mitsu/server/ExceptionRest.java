package fr.tokazio.mitsu.server;

public class ExceptionRest {

    private final String exceptionClassName;
    private final String message;
    private final Exception details;

    public ExceptionRest(Exception ex) {
        super();
        this.exceptionClassName = ex.getClass().getName();
        this.message = ex.getMessage();
        this.details = ex;
    }

    public String getExceptionClassName() {
        return exceptionClassName;
    }

    public String getMessage() {
        return message;
    }

    public Exception getDetails() {
        return details;
    }
}
