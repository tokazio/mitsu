package fr.tokazio.mitsu.server;

import io.netty.handler.codec.http.HttpMethod;

import java.util.Objects;
import java.util.regex.Pattern;

public class Route implements Comparable<Route> {

    private final HttpMethod method;
    private final String path;
    private final Handler handler;
    private final Pattern pathAsRegex;
    private final int pathLen;

    public Route(final HttpMethod method, final String path, final Handler handler) {
        if (path.trim().isEmpty()) {
            this.path = "/";
        } else {
            this.path = path;
        }
        this.method = method;
        this.handler = handler;
        this.pathAsRegex = Pattern.compile(this.path.replaceAll("\\{.+\\}", ".+"));
        this.pathLen = this.path.split("/").length;
    }

    public HttpMethod getMethod() {
        return method;
    }

    public String getPath() {
        return path;
    }

    public Handler getHandler() {
        return handler;
    }

    public boolean matches(final HttpMethod method, final String path) {
        return path.split("/").length == pathLen && pathAsRegex.matcher(path).matches();
    }

    @Override
    public int compareTo(Route o) {
        return -Integer.compare(path.split("/").length, o.path.split("/").length);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Route)) return false;
        Route route = (Route) o;
        return Objects.equals(path, route.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path);
    }
}