package fr.tokazio.mitsu.server;

public class HandlerException extends Exception {
    public HandlerException(Exception e) {
        super(e);
    }
}
