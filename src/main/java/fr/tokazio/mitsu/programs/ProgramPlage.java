package fr.tokazio.mitsu.programs;

import fr.tokazio.mitsu.core.AbstractProgram;
import fr.tokazio.mitsu.core.PlageHorraire;
import fr.tokazio.mitsu.core.ProgramVisitor;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public abstract class ProgramPlage<T> extends AbstractProgram<T> {

    private final List<PlageHorraire> plages = new ArrayList<>();

    public ProgramPlage(final String name) {
        super(name);
    }

    public ProgramPlage<T> addPlage(PlageHorraire plage) {
        plages.add(plage);
        return this;
    }

    public ProgramPlage<T> addPlages(List<PlageHorraire> plages) {
        this.plages.addAll(plages);
        return this;
    }

    @Override
    public void onStarting(ProgramVisitor visitor) {
        //rien à faire
    }

    @Override
    public void onTic(final ProgramVisitor visitor, final Calendar maintenant) {
        onTic();
        boolean auMoinsDansUnePlage = false;
        for (PlageHorraire plage : plages) {
            if (plage.contains(maintenant)) {
                auMoinsDansUnePlage = true;
                inPlage(plage);
                break;
            }
        }
        if (!auMoinsDansUnePlage) {
            outPlage();
        }
    }

    @Override
    public void onStopping(final ProgramVisitor visitor) {
        //rien à faire
    }

    protected abstract void onTic();

    protected abstract void inPlage(final PlageHorraire plageHorraire);

    protected abstract void outPlage();


}
