package fr.tokazio.mitsu.programs;

import fr.tokazio.mitsu.core.AbstractProgram;
import fr.tokazio.mitsu.core.ProgramVisitor;
import fr.tokazio.mitsu.core.StartingException;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class ScheduledProgram<T> extends AbstractProgram<T> {

    private final int ms;
    private final Calendar nextTic = Calendar.getInstance();
    private Consumer<T> consumer;

    public ScheduledProgram(final String name, final long time, final TimeUnit timeUnit) {
        super(name);
        this.ms = (int) TimeUnit.SECONDS.convert(time, timeUnit);
    }


    private void next() {
        nextTic.add(Calendar.SECOND, ms);
    }

    @Override
    public void onStarting(final ProgramVisitor visitor) throws StartingException {
        //rien à faire
    }

    @Override
    public void onTic(final ProgramVisitor visitor, final Calendar maintenant) {
        if (maintenant.after(nextTic)) {
            if (consumer != null) {
                for (T obj : list) {
                    LOGGER.info("Executing scheduled for {} ...", obj);
                    consumer.accept(obj);
                }
            }
            next();
        }
    }

    @Override
    public void onStopping(final ProgramVisitor visitor) {
        //nothing special
    }

    public ScheduledProgram<T> execute(final Consumer<T> consumer) {
        this.consumer = consumer;
        return this;
    }
}
