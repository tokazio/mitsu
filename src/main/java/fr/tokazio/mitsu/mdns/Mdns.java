package fr.tokazio.mitsu.mdns;

import net.straylightlabs.hola.dns.Domain;
import net.straylightlabs.hola.sd.Instance;
import net.straylightlabs.hola.sd.Query;
import net.straylightlabs.hola.sd.Service;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Set;

public class Mdns {

    private static final Logger LOGGER = LoggerFactory.getLogger(Mdns.class);

    public static byte[] pad16(byte[] data) {
        int len = data.length;
        while (len % 16 != 0) {
            len++;
        }
        byte[] out = new byte[len];
        System.arraycopy(data, 0, out, 0, data.length);
        return out;
    }

    public void lookup() {
        //https://github.com/itead/Sonoff_Devices_DIY_Tools/blob/master/SONOFF%20DIY%20MODE%20Protocol%20Doc%20v1.4.md
        try {
            Service service = Service.fromName("_ewelink._tcp.");
            Query query = Query.createFor(service, Domain.LOCAL);
            Set<Instance> instances = query.runOnce();
            System.out.println("mDNS ewelink devices:");
            for (Instance instance : instances) {
                String iv = instance.lookupAttribute("iv").trim();
                String deviceId = instance.lookupAttribute("id");
                int port = instance.getPort();
                String ip = instance.getAddresses().stream().findFirst().get().getHostAddress();
                System.out.println("*" + instance.getName());
                System.out.println("\t" + deviceId);
                System.out.println("\t" + instance.lookupAttribute("type"));
                System.out.println("\t" + ip);
                System.out.println("\t" + port);
                System.out.println("\t" + iv + " (" + iv.getBytes().length + ")");
                System.out.println(instance);

                String data1 = instance.lookupAttribute("data1");
                String data2 = instance.lookupAttribute("data2");
                String data3 = instance.lookupAttribute("data3");

                String data = data1;
                if (data2 != null) {
                    data += data2;
                }
                if (data3 != null) {
                    data += data3;
                }

                System.out.println("Encrypted:\n" + data);
                final IvParameterSpec ivParameterSpec = new IvParameterSpec(b64Decode(iv).getBytes());
                System.out.println("Decrypted:\n" + decrypt(ivParameterSpec, data));
                System.out.println();

                //post 192.168.1.22:8081/zeroconf/info
                //header("accept", "application/json")
                //                    .header("Content-Type", "application/json; utf-8")
//body:
                //{
                //    "deviceid": "1001361756",
                //    "data": {        }
                //}

                String url = "http://" + ip + ":" + port + "/zeroconf/switch";

                System.out.println("Getting infos from POST: " + url);

                Request req = new Request.Builder()
                        .url(url)
                        .post(RequestBody.create("{\n" +
                                "                    \"deviceid\": \"" + deviceId + "\",\n" +
                                "                    \"data\": {  \"switch\": \"on\"      }\n" +
                                "                }", MediaType.parse("application/json")))
                        .addHeader("accept", "application/json")
                        .addHeader("Content-Type", "application/json; utf-8")
                        .build();

                try {
                    Call call = new OkHttpClient().newCall(req);
                    Response response = call.execute();
                    System.out.println("Response " + response.code() + ":");
                    System.out.println(response.body().string());
                } catch (SocketTimeoutException e) {
                    System.out.println("TIME OUT " + url);
                }
            }
            System.out.println();
        } catch (IOException e) {
            LOGGER.error("mDNS error: ", e);
        }
    }

    private String b64Decode(final String in) {
        return new String(Base64.getDecoder().decode(in), StandardCharsets.US_ASCII);
    }

    //https://www.baeldung.com/java-aes-encryption-decryption
    //https://github.com/mattsaxon/Sonoff_Devices_DIY_Tools/commit/677826accf7d580195bc905e55d4164f86f0b47a
    public String decrypt(final IvParameterSpec iv, final String encodedString) {
        try {
            final String decodedb64String = b64Decode(encodedString);
            SecretKey key = KeyGenerator.getInstance("AES").generateKey();
            //  byte[] keyBytes = hashApiKeyToMD5();
            //  SecretKey originalKey = new SecretKeySpec(keyBytes, 0, keyBytes.length, "AES");

            return decrypt(decodedb64String, key, iv);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "error";
        /*
        def decrypt(self, data_element, iv):

        from Crypto.Hash import MD5
        from Crypto.Cipher import AES
        from Crypto.Util.Padding import pad, unpad
        from base64 import b64decode

        if iv is None:
            return eval(str(data_element, encoding="utf8"))
        else:

            ApiKey = [INSERT_TEST_API_KEY_HERE]
            encoded =  data_element

            h = MD5.new()
            h.update(ApiKey)
            key = h.digest()

            cipher = AES.new(key, AES.MODE_CBC, iv=b64decode(iv))
            ciphertext = b64decode(encoded)
            padded = cipher.decrypt(ciphertext)
            plaintext = unpad(padded, AES.block_size)

            print(plaintext)
            return eval(plaintext)
         */
    }

    private byte[] hashApiKeyToMD5() {
        return new byte[0];
    }

    private String decrypt(final String cipherText,
                           final SecretKey key,
                           final IvParameterSpec iv) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, key, iv);
        byte[] plainText = cipher.doFinal(pad16(cipherText.getBytes(StandardCharsets.US_ASCII)));
        return new String(plainText);
    }

}
