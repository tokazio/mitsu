package fr.tokazio.mitsu;

import fr.tokazio.mitsu.core.Condition;
import fr.tokazio.mitsu.core.InvalidCondition;
import fr.tokazio.mitsu.core.PlageHorraire;
import fr.tokazio.mitsu.mitsubishi.api.Clim;
import fr.tokazio.mitsu.programs.ProgramPlage;

import java.util.List;

public class CustomProgramPlageClim extends ProgramPlage<Clim> {

    public static final String EN_MARCHE = "en marche";
    public static final String A_L_ARRET = "à l'arrêt";

    private final List<Condition<Clim>> startingConditions =
            List.of(
                    Condition.<Clim>create("La clim doit être à l'arrêt").verifyWith(Clim::isNotPowered),
                    Condition.<Clim>create("La température intérieur doit être inférieure à 20°C").verifyWith(clim -> clim.hasRoomTemperatureUnder("20.0")),
                    Condition.<Clim>create("La température extérieur doit être supérieure à -6°C").verifyWith(clim -> clim.hasExternalTemperatureHigherThan("-6.0"))
            );

    private final List<Condition<Clim>> stoppingConditions =
            List.of(
                    Condition.<Clim>create("La clim doit être en marche").verifyWith(Clim::isPowered)
            );

    public CustomProgramPlageClim(final String name) {
        super(name);
    }

    @Override
    protected void onTic() {
        for (Clim clim : list) {
            LOGGER.info("* {} est {}", clim.getName(), (clim.isPowered() ? EN_MARCHE : A_L_ARRET));
        }
    }

    @Override
    protected void inPlage(final PlageHorraire plageHorraire) {
        LOGGER.info("Plage détectée: {}", plageHorraire);
        for (Clim clim : list) {
            LOGGER.info("* {} est {}  il y fait {}°C et il fait {}°C dehors", clim.getName(), (clim.isPowered() ? EN_MARCHE : A_L_ARRET), clim.getRoomTemperature(), clim.getExternalTemperature());
            try {
                verifyForStarting(clim);
                powerOn(clim);
            } catch (InvalidCondition e) {
                LOGGER.debug("Error validating conditions for {}", clim.getName(), e);
            }
        }
    }

    @Override
    protected void outPlage() {
        LOGGER.info("Hors plage: ");
        for (Clim clim : list) {
            LOGGER.info("* {} est {}  il y fait {}°C et il fait {}°C dehors", clim.getName(), (clim.isPowered() ? EN_MARCHE : A_L_ARRET), clim.getRoomTemperature(), clim.getExternalTemperature());
            try {
                verifyForStopping(clim);
                powerOff(clim);
            } catch (InvalidCondition e) {
                LOGGER.debug("Error validating conditions for {}", clim.getName(), e);
            }
        }
    }


    void verifyForStarting(final Clim clim) throws InvalidCondition {
        for (final Condition<Clim> cond : startingConditions) {
            if (!cond.verified(clim)) {
                throw new InvalidCondition(cond);
            }
        }
    }

    void verifyForStopping(final Clim clim) throws InvalidCondition {
        for (final Condition<Clim> cond : stoppingConditions) {
            if (!cond.verified(clim)) {
                throw new InvalidCondition(cond);
            }
        }
    }

    void powerOn(final Clim clim) {
        LOGGER.info("On démarre le chauffage");
        clim.powerOn(clim.getAskedTemperature());
    }

    void powerOff(final Clim clim) {
        LOGGER.info("{} s'arrête", clim.getName());
        clim.powerOff();
    }
}
