package fr.tokazio.mitsu.mitsubishi.api;

import fr.tokazio.mitsu.core.Power;
import fr.tokazio.mitsu.core.Temperature;
import fr.tokazio.mitsu.mitsubishi.webapi.State;
import fr.tokazio.mitsu.mitsubishi.webapi.WeatherObservation;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

public class TestDevice implements DeviceOldIntf {

    private static final BigDecimal deg19 = new BigDecimal("19.0");
    private static final BigDecimal degMinus5 = new BigDecimal("-5.0");


    private final State state = new State();

    public TestDevice() {
        state.setRoomTemperature(deg19);
        state.setWeatherObservations(List.of(
                woMinus5()
        ));
    }

    private final WeatherObservation woMinus5() {
        WeatherObservation out = new WeatherObservation();
        out.setTemperature(degMinus5);
        return out;
    }

    @Override
    public void power(final Power power, final Temperature temp) throws IOException {
        state.setPower(power.asBoolean());
    }

    public State state() {
        return state;
    }
}
