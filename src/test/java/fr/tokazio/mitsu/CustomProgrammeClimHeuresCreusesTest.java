package fr.tokazio.mitsu;

import fr.tokazio.mitsu.core.PlageHorraire;
import fr.tokazio.mitsu.core.Power;
import fr.tokazio.mitsu.core.ProgrammeException;
import fr.tokazio.mitsu.core.StartingException;
import fr.tokazio.mitsu.mitsubishi.MelCloudClient2;
import fr.tokazio.mitsu.mitsubishi.api.Clim;
import fr.tokazio.mitsu.mitsubishi.api.DeviceOldIntf;
import fr.tokazio.mitsu.mitsubishi.api.TestDevice;
import fr.tokazio.mitsu.mitsubishi.webapi.DeviceNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class CustomProgrammeClimHeuresCreusesTest {

    private static final BigDecimal deg21 = new BigDecimal("21.0");
    private static final BigDecimal degMinus7 = new BigDecimal("-7.0");

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

    private final MelCloudClient2 mockedClient = Mockito.mock(MelCloudClient2.class);

    private final CustomProgramPlageClim prog = Mockito.spy(new CustomProgramPlageClim("Plages de test")
                    .addPlage(PlageHorraire.of("12:30", "14:30")))
            .addAll(List.of(
                    Clim.of(mockedClient, "Piéce A"),
                    Clim.of(mockedClient, "B")));

    private final DeviceOldIntf pieceADevice = testDevice();
    private final DeviceOldIntf bDevice = testDevice();

    @BeforeEach
    public void setup() throws StartingException, DeviceNotFoundException, IOException {
        Mockito.doReturn(pieceADevice).when(mockedClient).find("Piéce A");
        Mockito.doReturn(bDevice).when(mockedClient).find("B");
        prog.onStarting(mockedClient);
    }


    @Test
    void toutALarretA12h00() throws ProgrammeException, ParseException, IOException {
        //given

        //when
        prog.onTic(mockedClient, get("12:00"));

        //then
        Mockito.verify(prog, Mockito.never()).inPlage(Mockito.any());
        Mockito.verify(prog, Mockito.times(1)).outPlage();
    }

    @Test
    void toutALarretA13h00TempExtMinus7() throws ProgrammeException, ParseException, IOException {
        //given
        pieceADevice.state().externalTemperature(degMinus7);
        bDevice.state().externalTemperature(degMinus7);

        //when
        prog.onTic(mockedClient, get("13:00"));

        //then
        Mockito.verify(prog, Mockito.times(1)).inPlage(Mockito.any());
        Mockito.verify(prog, Mockito.never()).outPlage();

        assertThat(pieceADevice.state().isPowered()).as("Pièce A not starting because of external temperature minus 7").isFalse();
        assertThat(bDevice.state().isPowered()).as("B not starting because of external temperature minus 7").isFalse();
    }

    @Test
    void toutALarretA13h00TempRoom21() throws ProgrammeException, ParseException, IOException {
        //given
        pieceADevice.state().roomTemperature(deg21);
        bDevice.state().roomTemperature(deg21);

        //when
        prog.onTic(mockedClient, get("13:00"));

        //then
        Mockito.verify(prog, Mockito.times(1)).inPlage(Mockito.any());
        Mockito.verify(prog, Mockito.never()).outPlage();

        assertThat(pieceADevice.state().isPowered()).as("Pièce A not starting because of room temperature 21").isFalse();
        assertThat(bDevice.state().isPowered()).as("B not starting because of room temperature 21").isFalse();
    }

    @Test
    void unEnMarcheA12h29() throws ProgrammeException, ParseException, IOException {
        //given
        pieceADevice.state().power(Power.ON.asBoolean());

        //when
        prog.onTic(mockedClient, get("12:29"));

        //then
        Mockito.verify(prog, Mockito.never()).inPlage(Mockito.any());
        Mockito.verify(prog, Mockito.times(1)).outPlage();

        Mockito.verify(prog, Mockito.times(1)).powerOff(Mockito.any());

        assertThat(pieceADevice.state().isPowered()).as("Pièce A was stopped because it's powered out of the current plage").isFalse();

    }


    @Test
    void unEnMarcheA12h30() throws ProgrammeException, ParseException, IOException {
        //given
        pieceADevice.state().power(Power.ON.asBoolean());

        //when)
        prog.onTic(mockedClient, get("12:30"));

        //then
        Mockito.verify(prog, Mockito.times(1)).inPlage(Mockito.any());
        Mockito.verify(prog, Mockito.never()).outPlage();

        Mockito.verify(prog, Mockito.times(1)).powerOn(Mockito.any());

        assertThat(bDevice.state().isPowered()).as("B was started because not powered in the current plage").isTrue();
    }

    @Test
    void unEnMarcheA14h31() throws ProgrammeException, ParseException, IOException {
        //given
        pieceADevice.state().power(Power.ON.asBoolean());

        //when)
        prog.onTic(mockedClient, get("14:31"));

        //then
        Mockito.verify(prog, Mockito.never()).inPlage(Mockito.any());
        Mockito.verify(prog, Mockito.times(1)).outPlage();

        Mockito.verify(prog, Mockito.times(1)).powerOff(Mockito.any());

        assertThat(pieceADevice.state().isPowered()).as("Pièce A was stopped because it's powered out of the current plage").isFalse();
    }

    private DeviceOldIntf testDevice() {
        return new TestDevice();
    }

    private Calendar get(final String hoursMinutes) throws ParseException {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(dateFormat.parse(hoursMinutes));
        return cal;
    }

}
