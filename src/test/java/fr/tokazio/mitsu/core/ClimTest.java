package fr.tokazio.mitsu.core;

import fr.tokazio.mitsu.mitsubishi.api.Clim;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class ClimTest {

    private final Clim clim = Mockito.spy(Clim.of("test1"));

    @BeforeEach
    public void setup() {
        Mockito.doReturn(new BigDecimal("19.00")).when(clim).getRoomTemperature();
        Mockito.doReturn(new BigDecimal("-7.00")).when(clim).getExternalTemperature();
    }

    @Test
    void RoomTemperatureUnder20() {
        //given

        //when
        final boolean actual = clim.hasRoomTemperatureUnder("20.00");

        //then
        assertThat(actual).isTrue();
    }

    @Test
    void roomTemperatureUnder18() {
        //given

        //when
        final boolean actual = clim.hasRoomTemperatureUnder("18.00");

        //then
        assertThat(actual).isFalse();
    }

    @Test
    void hasExternalTemperatureHigherThanMinus6() {
        //given

        //when
        final boolean actual = clim.hasExternalTemperatureHigherThan("-6.00");

        //then
        assertThat(actual).isFalse();
    }

    @Test
    void hasExternalTemperatureHigherThanMinus8() {
        //given

        //when
        final boolean actual = clim.hasExternalTemperatureHigherThan("-8.00");

        //then
        assertThat(actual).isTrue();
    }
}
