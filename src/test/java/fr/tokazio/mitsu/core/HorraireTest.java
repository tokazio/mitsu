package fr.tokazio.mitsu.core;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static org.assertj.core.api.Assertions.assertThat;

class HorraireTest {

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

    private final Horraire horraire = Horraire.of(10, 50);

    @ParameterizedTest
    @ValueSource(strings = {"12:00", "10:51"})
    void h10m50LowerOrEquals_true(final String in) throws ParseException {
        //given

        //when
        final boolean actual = horraire.lowerOrEquals(get(in));

        //then
        assertThat(actual).isTrue();
    }

    @ParameterizedTest
    @ValueSource(strings = {"10:00", "09:24"})
    void h10m50GreaterThan_true(final String in) throws ParseException {
        //given

        //when
        final boolean actual = horraire.greaterThan(get(in));

        //then
        assertThat(actual).isTrue();
    }

    @Test
    void h10m50NotGreaterThan10h51m() throws ParseException {
        //given

        //when
        final boolean actual = horraire.greaterThan(get("10:51"));

        //then
        assertThat(actual).isFalse();
    }

    private Calendar get(final String hoursMinutes) throws ParseException {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(dateFormat.parse(hoursMinutes));
        return cal;
    }


}
