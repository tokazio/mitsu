package fr.tokazio.mitsu.core;

import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static org.assertj.core.api.Assertions.assertThat;

class PlageHorraireTest {

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

    private final PlageHorraire plage = PlageHorraire.of("12:30", "14:30");

    @Test
    void plage12h30to14h30Contains12h31() throws ParseException {
        //given

        //when
        final boolean actual = plage.contains(get("12:30"));

        //then
        assertThat(actual).isTrue();
    }

    @Test
    void plage12h30to14h30Contains14h29() throws ParseException {
        //given

        //when
        final boolean actual = plage.contains(get("14:29"));

        //then
        assertThat(actual).isTrue();
    }

    @Test
    void plage12h30to14h30NotContains14h30() throws ParseException {
        //given

        //when
        final boolean actual = plage.contains(get("14:30"));

        //then
        assertThat(actual).isFalse();
    }

    @Test
    void plage12h30to14h30NotContains15h27() throws ParseException {
        //given

        //when
        final boolean actual = plage.contains(get("15:27"));

        //then
        assertThat(actual).isFalse();
    }

    private Calendar get(final String hoursMinutes) throws ParseException {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(dateFormat.parse(hoursMinutes));
        return cal;
    }
}
